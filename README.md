# HomeWork

Minimal RESTful web service.

## Installation
Make sure you have [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) installed.
```bash
git clone https://silvav@bitbucket.org/silvav/homework.git homework

# It can take a while for the execution environment to become ready
cd homework && make install
```

## Test suites
Run Phpunit tests by issuing the following command
```bash
cd homework && make phpunit
```
Run Behat scenarios by issuing the following command
```bash
cd homework && make behat
```
## Assumptions
- API should be publicly accessible ``` TRUSTED_HOSTS='^.*$ ' ```
- All product properties must be set. Empty values are not allowed.

## Limitations and improvements
- Auto generated documentation, ie.: [NelmioApiDocBundle](https://github.com/nelmio/NelmioApiDocBundle)
- Pagination for resource collections retrieval. ie.: [Fractal](https://fractal.thephpleague.com/pagination/)
- Applying caching, [Richardon Maturity Model | level 2](https://martinfowler.com/articles/richardsonMaturityModel.html#level2)
- Considering Hypermedia Controls, [Richardon Maturity Model | level 3](https://martinfowler.com/articles/richardsonMaturityModel.html#level3)
- Conformance to API description formats, ie.: [OAS](https://www.openapis.org/), [JSON-LD](https://json-ld.org/)
- Cache country code resolutions
- Log country resolver failures
