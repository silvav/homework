<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190514084726 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE INDEX product_type_idx ON product (product_type)');
        $this->addSql('CREATE UNIQUE INDEX product_unq ON product (product_type, color, size)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP INDEX product_type_idx ON product');
        $this->addSql('DROP INDEX product_unq ON product');
    }
}
