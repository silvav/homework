<?php

namespace App\Controller;

use App\Service\Order\OrderManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class OrderController
{
    /**
     * @Route("/orders", methods={"POST"})
     *
     * @param Request $request
     * @param OrderManager $orderManager
     * @return Response
     */
    public function post(Request $request, OrderManager $orderManager): Response
    {
        $requestContent = $request->getContent();
        $requestItems = json_decode($requestContent, true);

        $clientIp = $request->getClientIp();
        $viewModel = $orderManager->create($requestItems, $clientIp);

        return new JsonResponse($viewModel->getData(), $viewModel->getCode());
    }

    /**
     * @Route("/orders", methods={"GET"})
     *
     * @param Request $request
     * @param OrderManager $orderManager
     * @return Response
     */
    public function cget(Request $request, OrderManager $orderManager): Response
    {
        $productType = $request->query->get('product_type', '');
        $criteria = [];
        if ('' !== $productType) {
            $criteria['product_type'] = $productType;
        }

        $viewModel = $orderManager->findAll($criteria);

        return new JsonResponse($viewModel->getData(), $viewModel->getCode());
    }
}
