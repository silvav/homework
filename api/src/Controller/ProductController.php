<?php

namespace App\Controller;

use App\Model\Product\ProductDTO;
use App\Service\Product\ProductManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class ProductController
{
    /**
     * @Route("/products", methods={"POST"})
     *
     * @param Request $request
     * @param ProductManager $productManager
     * @return Response
     */
    public function post(Request $request, ProductManager $productManager): Response
    {
        $requestContent = $request->getContent();
        $resourceRequest = json_decode($requestContent, true);

        $productDTO = ProductDTO::fromArray($resourceRequest);
        $viewModel = $productManager->create($productDTO);

        return new JsonResponse($viewModel->getData(), $viewModel->getCode());
    }
}
