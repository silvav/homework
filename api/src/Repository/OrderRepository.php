<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @param string $productType
     * @return array|Order[]
     */
    public function findAllByProductType(string $productType): array
    {
        return $this->createQueryBuilder('o')
            ->innerJoin('o.orderItems', 'oi')
            ->innerJoin('oi.product', 'p')
            ->addSelect('o')
            ->andWhere('p.productType = :productType')
            ->setParameter('productType', $productType)
            ->getQuery()
            ->getArrayResult();
    }

    public function findByOriginCountryAndCreatedAtPeriod(string $originCountry, \DateTimeInterface $startAt): int
    {
        $qb = $this->createQueryBuilder('o');
        $query = $qb->select('count(o.id)')
            ->andWhere('o.originCountry = :originCountry')
            ->andWhere($qb->expr()->gte('o.createdAt', ':createdAt'))
            ->setParameter('originCountry', $originCountry)
            ->setParameter('createdAt', $startAt)
            ->getQuery();

        return (int) $query->getSingleScalarResult();
    }
}
