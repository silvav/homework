<?php

namespace App\Model;

use App\Constants\ResponseCode;

class ViewModel
{
    /**
     * @var mixed
     */
    private $data;

    /**
     * @var int
     */
    private $code;

    /**
     * ViewModel constructor.
     * @param mixed $data
     * @param int $code
     */
    public function __construct($data, int $code = ResponseCode::OK)
    {
        $this->data = $data;
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }
}