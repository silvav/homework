<?php

namespace App\Model\Product;

class ProductDTO
{
    /**
     * @var string
     */
    private $price;

    /**
     * @var string
     */
    private $size;

    /**
     * @var string
     */
    private $color;

    /**
     * @var string
     */
    private $productType;

    private function __construct()
    {
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @return string
     */
    public function getProductType(): string
    {
        return $this->productType;
    }

    public static function fromArray(array $productData): self
    {
        $instance = new self();
        $instance->price = (string) ($productData['price'] ?? '');
        $instance->size = (string) ($productData['size'] ?? '');
        $instance->color = (string) ($productData['color'] ?? '');
        $instance->productType = (string) ($productData['product_type'] ?? '');

        return $instance;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}