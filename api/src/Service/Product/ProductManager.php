<?php

namespace App\Service\Product;

use App\Constants\ResponseCode;
use App\Entity\Product;
use App\Model\Product\ProductDTO;
use App\Model\ViewModel;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;

class ProductManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(ProductDTO $productDTO): ViewModel
    {
        if ($this->hasEmptyProperty($productDTO)) {
            $data = ['errors' => [['title' => 'Bad data']]];
            return new ViewModel($data, ResponseCode::UNPROCESSABLE_ENTITY);
        }

        $product = $this->createProduct($productDTO);

        try {
            $this->entityManager->persist($product);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $exception) {
            $data = ['errors' => [['title' => 'Uniqueness violation']]];
            return new ViewModel($data, ResponseCode::UNPROCESSABLE_ENTITY);
        }

        return new ViewModel($product, ResponseCode::CREATED);
    }

    /**
     * @param ProductDTO $productDTO
     * @return Product
     */
    public function createProduct(ProductDTO $productDTO): Product
    {
        $product = (new Product())
            ->setPrice($productDTO->getPrice())
            ->setSize($productDTO->getSize())
            ->setColor($productDTO->getColor())
            ->setProductType($productDTO->getProductType());

        return $product;
    }

    /**
     * @param ProductDTO $productDTO
     * @return bool
     */
    private function hasEmptyProperty(ProductDTO $productDTO): bool
    {
        return in_array('', $productDTO->toArray(), true);
    }
}