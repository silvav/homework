<?php

namespace App\Service\Clock;

class Clock implements ClockInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDateTime($time = 'now', $timezone = NULL): \DateTimeInterface
    {
        return new \DateTime($time, $timezone);
    }
}