<?php

namespace App\Service\Clock;

interface ClockInterface
{
    /**
     * @param string|null $time
     * @param \DateTimeZone $timezone|null
     * @return \DateTimeInterface
     */
    public function getDateTime($time = 'now', $timezone = NULL): \DateTimeInterface;
}