<?php

namespace App\Service\Order;

use App\Entity\Order;
use App\Repository\OrderRepository;
use App\Service\Clock\ClockInterface;

class OrderRateLimiter
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var ClockInterface
     */
    private $clock;

    /**
     * @var int
     */
    private $period;

    /**
     * @var int
     */
    private $threshold;

    public function __construct(
        OrderRepository $orderRepository,
        ClockInterface $clock,
        int $period,
        int $threshold
    ) {
        $this->orderRepository = $orderRepository;
        $this->period = $period;
        $this->threshold = $threshold;
        $this->clock = $clock;
    }

    public function isLimitedByCountryOfOrigin(Order $order): bool
    {
        if ($this->threshold < 0) {
            return false;
        }

        $originCountry = $order->getOriginCountry();
        $startAt = $this->getStartAt();

        $orderCount = $this->orderRepository->findByOriginCountryAndCreatedAtPeriod(
            $originCountry,
            $startAt
        );

        return $orderCount >= $this->threshold;
    }

    public function getStartAt(): \DateTimeInterface
    {
        $diff = sprintf('-%s seconds', $this->period);
        return $this->clock->getDateTime()->modify($diff);
    }
}