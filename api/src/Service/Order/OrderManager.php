<?php

namespace App\Service\Order;

use App\Constants\ResponseCode;
use App\Model\ViewModel;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Service\ApiClient\CountryResolver;
use Doctrine\ORM\EntityManagerInterface;

class OrderManager
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var CountryResolver
     */
    private $countryResolver;

    /**
     * @var OrderFactory
     */
    private $orderFactory;
    /**
     * @var OrderRateLimiter
     */
    private $orderRateLimiter;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository,
        CountryResolver $countryResolver,
        OrderFactory $orderFactory,
        OrderRateLimiter $orderRateLimiter,
        OrderRepository $orderRepository
    ) {

        $this->productRepository = $productRepository;
        $this->countryResolver = $countryResolver;
        $this->orderFactory = $orderFactory;
        $this->orderRateLimiter = $orderRateLimiter;
        $this->entityManager = $entityManager;
        $this->orderRepository = $orderRepository;
    }

    public function create(array $requestItems, string $clientIp): ViewModel
    {
        if (empty($requestItems)) {
            return $this->getErrorResponse('Empty order');
        }

        try {
            $orderItems = $this->assembleOrderItems($requestItems);
            $countryCode = $this->countryResolver->resolveCode($clientIp);

            $order = $this->orderFactory->create($orderItems);
            $order->setOriginCountry($countryCode);
        } catch (\DomainException $exception) {
            return $this->getErrorResponse($exception->getMessage());
        }

        if ($this->orderRateLimiter->isLimitedByCountryOfOrigin($order)) {
            return $this->getErrorResponse('Try again later');
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return new ViewModel($order, ResponseCode::CREATED);
    }

    /**
     * @param array $criteria
     * @return ViewModel
     */
    public function findAll(array $criteria): ViewModel
    {
        $productType = (string) ($criteria['product_type'] ?? '');
        if ('' !== $productType) {
            return new ViewModel($this->orderRepository->findAllByProductType($productType));
        }

        return new ViewModel($this->orderRepository->findAll());
    }

    /**
     * @param array $requestItems
     * @throws \DomainException
     * @return array
     */
    private function assembleOrderItems(array $requestItems): array
    {
        $orderItems = [];
        foreach ($requestItems as $requestItem) {
            $product = $this->productRepository->find(['id' => $requestItem['id']]);
            if (null === $product) {
                throw new \DomainException('Product not found, identifier: ' . $requestItem['id']);
            }
            $orderItem['product'] = $product;

            $orderItem['quantity'] = $requestItem['quantity'];
            $orderItems[] = $orderItem;
        }
        return $orderItems;
    }

    private function getErrorResponse($message, $code = ResponseCode::UNPROCESSABLE_ENTITY): ViewModel
    {
        $response = ['errors' => [['title' => $message]]];

        return new ViewModel($response, $code);
    }
}
