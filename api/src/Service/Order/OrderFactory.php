<?php

namespace App\Service\Order;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use Zend\EventManager\Exception\DomainException;

class OrderFactory
{
    public const MINIMUM_TOTAL_PRICE = 10;

    public function create(array $items): Order
    {
        $order = new Order();
        $order->setStatus('draft');

        foreach ($items as $item) {
            $orderItem = new OrderItem();
            /** @var Product $product */
            $product = $item['product'];
            $orderItem->setProduct($item['product']);

            $quantity = $item['quantity'];
            $orderItem->setQuantity($quantity);

            $price = bcmul((string) $quantity, $product->getPrice(), 2);
            $orderItem->setTotalPrice($price);

            $order->addOrderItem($orderItem);
            $orderItem->setOrder($order);
        }

        if ($order->getTotalPrice() < self::MINIMUM_TOTAL_PRICE) {
            throw new DomainException(
                sprintf('Minimum total price is lower than %f', self::MINIMUM_TOTAL_PRICE)
            );
        }

        $dateTime = new \DateTimeImmutable('now');
        $order->setCreatedAt($dateTime);

        return $order;
    }
}
