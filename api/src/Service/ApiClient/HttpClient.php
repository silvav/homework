<?php

namespace App\Service\ApiClient;

use GuzzleHttp\ClientInterface as GuzzleClient;

class HttpClient implements ClientInterface
{
    /**
     * @var GuzzleClient
     */
    private $client;

    public function __construct(GuzzleClient $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritdoc}
     */
    public function request($method, $uri, array $options = [])
    {
        return $this->client->request($method, $uri, $options);
    }
}