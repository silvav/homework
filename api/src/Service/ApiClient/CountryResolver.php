<?php

namespace App\Service\ApiClient;

use Psr\Http\Message\ResponseInterface;

class CountryResolver
{
    public const DEFAULT_COUNTRY_CODE = 'US';
    private const URI_FORMAT = 'https://ipapi.co/%s/country/';

    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientProxy $client)
    {
        $this->client = $client;
    }

    public function resolveCode(string $ipAddress): string
    {
        $uri = sprintf(self::URI_FORMAT, $ipAddress);
        $response = $this->client->request('GET', $uri);

        if (200 !== $response->getStatusCode()) {
            // @TODO log failures, it is important to know reliability of external service on prod
            return self::DEFAULT_COUNTRY_CODE;
        }

        $responseBody = (string) $response->getBody();
        if ($this->isValidCountryCode($responseBody)) {
            return self::DEFAULT_COUNTRY_CODE;
        }

        return $responseBody;
    }

    private function isValidCountryCode(string $responseBody): bool
    {
        return 'undefined' === mb_strtolower($responseBody) && strlen($responseBody) > 2;
    }
}
