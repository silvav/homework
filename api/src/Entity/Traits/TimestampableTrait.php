<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait TimestampableTrait
{
    /**
     * @var \DateTimeImmutable $createdAt Created at
     *
     * @ORM\Column(type="datetime_immutable", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTimeImmutable $updatedAt Updated at
     *
     * @ORM\Column(type="datetime_immutable", name="updated_at")
     */
    protected $updatedAt;

    /**
     * Set created at
     *
     * @param \DateTimeInterface $dateTime
     *
     * @return $this
     */
    public function setCreatedAt(\DateTimeInterface $dateTime = null): self
    {
        if (null === $dateTime) {
            $dateTime = new \DateTimeImmutable('now');
        }
        if ($dateTime instanceof \DateTime) {
            $dateTime = \DateTimeImmutable::createFromMutable($dateTime);
        }

        $this->createdAt = $dateTime;
        $this->updatedAt = $dateTime;

        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Set updated at
     *
     * @param \DateTimeInterface $dateTime
     *
     * @return $this
     */
    public function setUpdatedAt(\DateTimeInterface $dateTime = null): self
    {
        if (null === $dateTime) {
            $dateTime = new \DateTimeImmutable('now');
        }
        if ($dateTime instanceof \DateTime) {
            $dateTime = \DateTimeImmutable::createFromMutable($dateTime);
        }

        $this->updatedAt = $dateTime;

        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }
}