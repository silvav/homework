<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 */
class OrderItem implements \JsonSerializable
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderItems")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getTotalPrice(): string
    {
        return bcdiv((string) $this->price, '100', 2);
    }

    public function setTotalPrice(string $price): self
    {
        $this->price = (int) bcmul($price, '100', 0);
        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity($quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id'           => $this->getId(),
            'price'        => (float) $this->getTotalPrice(),
            'quantity'     => $this->getQuantity(),
            'product_type' => $this->getProduct()->getProductType(),
            'color'        => $this->getProduct()->getColor(),
            'size'         => $this->getProduct()->getSize(),
        ];
    }

    public function __toString()
    {
        return self::class;
    }
}
