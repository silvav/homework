<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use App\Entity\Traits\TimestampableTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="app_order")
 */
class Order implements \JsonSerializable
{
    use TimestampableTrait;

    public const DEFAULT_ORIGIN_COUNTRY = 'US';

    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="order", cascade={"persist"})
     */
    private $orderItems;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $originCountry;

    public function __construct() {
        $this->orderItems = new ArrayCollection();
        $this->setOriginCountry(self::DEFAULT_ORIGIN_COUNTRY);
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus($status): self
    {
        $this->status = mb_strtolower(trim($status));
        return $this;
    }

    public function getOriginCountry(): string
    {
        return $this->originCountry;
    }

    public function setOriginCountry(string $originCountry): self
    {
        $this->originCountry = mb_strtoupper(trim($originCountry));
        return $this;
    }

    /**
     * @return array|OrderItem[]
     */
    public function getItems(): array
    {
        return $this->orderItems->toArray();
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        $this->orderItems->add($orderItem);
        return $this;
    }

    public function getTotalPrice(): string
    {
        $sum = static function($carry, OrderItem $orderItem)
        {
            $carry = bcadd($orderItem->getTotalPrice(), $carry, 2);
            return $carry;
        };

        return array_reduce($this->getItems(), $sum);
    }

    public function jsonSerialize()
    {
        return [
            'id'             => $this->getId(),
            'status'         => $this->getStatus(),
            'origin_country' => $this->getOriginCountry(),
            'items'          => $this->getItems(),
            'total_price'    => (float) $this->getTotalPrice(),
            'created_at'     => $this->getCreatedAt()->format('c'),
            'updated_at'     => $this->getUpdatedAt()->format('c'),
        ];
    }
}
