<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(
 *   uniqueConstraints={
 *     @ORM\UniqueConstraint(
 *       name="product_unq",
 *       columns={"product_type", "color", "size"}
 *     )
 *   },
 *   indexes={
 *      @ORM\Index(name="product_type_idx", columns={"product_type"})
 *   }
 * )
 */
class Product implements \JsonSerializable
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $productType;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $size;

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getPrice(): string
    {
        return bcdiv((string) $this->price, '100', 2);
    }

    public function setPrice(string $price): self
    {
        $this->price = (int) bcmul($price, '100', 0);
        return $this;
    }

    public function getProductType(): ?string
    {
        return $this->productType;
    }

    public function setProductType(string $productType): self
    {
        $this->productType = mb_strtolower(trim($productType));
        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = mb_strtolower(trim($color));
        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = mb_strtolower(trim($size));
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id'           => $this->getId(),
            'price'        => $this->getPrice(),
            'product_type' => $this->getProductType(),
            'color'        => $this->getColor(),
            'size'         => $this->getSize(),
        ];
    }

    public function __toString()
    {
       return (string) $this->getId();
    }
}
