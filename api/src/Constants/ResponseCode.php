<?php

namespace App\Constants;

final class ResponseCode
{
    public const OK = 200;
    public const CREATED = 201;
    public const UNPROCESSABLE_ENTITY = 422;
}