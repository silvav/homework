<?php

namespace App\Tests\Service\Order;

use App\Entity\Order;
use App\Repository\OrderRepository;
use App\Service\Order\OrderRateLimiter;
use App\Tests\Service\Clock\TimeTravelMachine;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @property OrderRepository|\PHPUnit\Framework\MockObject\MockObject orderRepository
 * @property TimeTravelMachine timeTravelMachine
 * @property int period
 * @property Order order
 * @property \DateTime now
 * @property string originCountry
 */
class OrderRateLimiterTest extends WebTestCase
{
    public function setUp()
    {
        $this->orderRepository = $this->createMock(OrderRepository::class);
        $this->timeTravelMachine = new TimeTravelMachine();
        $this->period = 3600;

        $this->originCountry = 'US';
        $this->now = new \DateTime('now');
        $this->order = new Order();
        $this->order->setOriginCountry($this->originCountry)
            ->setCreatedAt($this->now);
    }

    public function testNegativeThresholdShouldSkipRateLimiting(): void
    {
        $orderRateLimiter = new OrderRateLimiter(
            $this->orderRepository,
            $this->timeTravelMachine,
            $this->period,
            -1
        );

        $this->assertFalse($orderRateLimiter->isLimitedByCountryOfOrigin($this->order));
    }

    /**
     * Order count bigger or equal than threshold will limit
     *
     * @dataProvider positiveRateLimiterDataProvider
     * @param $threshold
     * @param $orderCount
     */
    public function testRateLimiterShouldLimit($threshold, $orderCount): void
    {
        $this->orderRepository->expects($this->atLeastOnce())
            ->method('findByOriginCountryAndCreatedAtPeriod')
            ->willReturn($orderCount);

        $this->timeTravelMachine->travelTo($this->now);

        $orderRateLimiter = new OrderRateLimiter(
            $this->orderRepository,
            $this->timeTravelMachine,
            $this->period,
            $threshold
        );

        $this->assertTrue($orderRateLimiter->isLimitedByCountryOfOrigin($this->order));
    }

    public function positiveRateLimiterDataProvider(): array
    {
        return [
            'same value will limit'         => [
                'threshold'   => 100,
                'order count' => 100
            ],
            'bigger order count will limit' => [
                'threshold'   => 100,
                'order count' => 111
            ],
        ];
    }

    /**
     * Order count lower than threshold will not limit
     *
     * @dataProvider negativeRateLimiterDataProvider
     * @param $threshold
     * @param $orderCount
     */
    public function testRateLimiterShouldNotLimit($threshold, $orderCount): void
    {
        $this->orderRepository->expects($this->atLeastOnce())
            ->method('findByOriginCountryAndCreatedAtPeriod')
            ->willReturn($orderCount);

        $this->timeTravelMachine->travelTo($this->now);

        $orderRateLimiter = new OrderRateLimiter(
            $this->orderRepository,
            $this->timeTravelMachine,
            $this->period,
            $threshold
        );

        $this->assertFalse($orderRateLimiter->isLimitedByCountryOfOrigin($this->order));
    }

    public function negativeRateLimiterDataProvider(): array
    {
        return [
            'lower value will not limit'         => [
                'threshold'   => 100,
                'order count' => 10
            ],
            'another lower count will not limit' => [
                'threshold'   => 100,
                'order count' => 90
            ],
        ];
    }

    public function testRateLimiterStartAtShouldReturnStartAtScanDate(): void
    {
        $testDateTime = new \DateTime('2018-10-10 10:10:10');
        $this->timeTravelMachine->travelTo($testDateTime);
        $period = 5;

        $orderRateLimiter = new OrderRateLimiter(
            $this->orderRepository,
            $this->timeTravelMachine,
            $period,
            10
        );

        $expectedStartAt = clone $testDateTime;
        $diff = sprintf('-%s seconds', $period);
        $expectedStartAt = $expectedStartAt->modify($diff);

        $this->assertEquals(
            $expectedStartAt,
            $orderRateLimiter->getStartAt()
        );
    }

}
