<?php

namespace App\Tests\Service\ApiClient;

use App\Service\ApiClient\ClientInterface;
use GuzzleHttp\Psr7\Response;

class ClientMock implements ClientInterface
{
    public const LV_IP = '217.195.48.1';
    public const FAILURE_IP = '666.666.666.666';

    private static $ipResolutions = [
        '217.195.48.1' => 'LV',
    ];

    /**
     * {@inheritdoc}
     */
    public function request($method, $uri, array $options = [])
    {
        preg_match('/\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/', $uri, $ipAddresses);
        if (empty($ipAddresses)) {
            throw new \DomainException('Unable to extract IP from URI');
        }

        $body = self::$ipResolutions[$ipAddresses[0]] ?? '';

        $status = 200;
        if ('' === $body) {
            $status = 400;
        }

        return new Response($status, [], $body);
    }
}