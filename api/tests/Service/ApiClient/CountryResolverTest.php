<?php

namespace App\Tests\Service\ApiClient;

use App\Service\ApiClient\ClientProxy;
use App\Service\ApiClient\CountryResolver;
use PHPUnit\Framework\TestCase;

class CountryResolverTest extends TestCase
{
    public function testCountryResolverShouldReturnCountryCode(): void
    {
        $clientMock = new ClientMock();
        $clientProxy = new ClientProxy($clientMock);
        $countryResolver = new CountryResolver($clientProxy);

        $countryCode = $countryResolver->resolveCode(ClientMock::LV_IP);

        $this->assertSame('LV', $countryCode);
    }

    public function testCountryResolverOnFailureShouldReturnDefaultCountryCode(): void
    {
        $clientMock = new ClientMock();
        $clientProxy = new ClientProxy($clientMock);
        $countryResolver = new CountryResolver($clientProxy);

        $countryCode = $countryResolver->resolveCode(ClientMock::FAILURE_IP);

        $this->assertSame(CountryResolver::DEFAULT_COUNTRY_CODE, $countryCode);
    }
}
