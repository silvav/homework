<?php

namespace App\Tests\Service\Clock;

use App\Service\Clock\ClockInterface;

class TimeTravelMachine implements ClockInterface
{

    /**
     * @var \DateTimeInterface
     */
    private $currentTime;

    public function __construct()
    {
        $this->currentTime = new \DateTime('now');
    }

    /**
     * {@inheritdoc}
     */
    public function getDateTime($time = 'now', $timezone = null): \DateTimeInterface
    {
        return $this->currentTime;
    }

    public function travelTo(\DateTimeInterface $dateTime): void
    {
        $this->currentTime = $dateTime;
    }
}