Feature: Products

  Scenario: Create product
    Given the request body is:
    """
    {
      "price": 9.99,
      "product_type": "mug",
      "color": "Blue",
      "size": "M"
    }
    """
    When I request "/products" using HTTP POST
    Then the response code is 201
    And response item "[id]" should exist
    And response item "[price]" should contain "9.99"
    And response item "[product_type]" should contain "mug"
    And response item "[color]" should contain "blue"
    And response item "[size]" should contain "m"

  Scenario: Ensure product uniqueness by type + color + size
    Given I create products:
      | id             | price | product_type | color  | size |
      | <product_id_1> | 5.25  | pin          | red    | XXS  |
    Given the request body is:
    """
    {
      "price": 9.99,
      "product_type": "Pin",
      "color": "Red",
      "size": "xxs"
    }
    """
    When I request "/products" using HTTP POST
    Then the response code is 422
    And response item "[errors]" should exist
    And response item "[errors][0][title]" should contain "Uniqueness violation"

  Scenario: Ensure all product properties are set and not empty
    Given the request body is:
    """
    {
      "price": 9.99,
      "product_type": "Pin",
      "color": "",
      "size": "xxs"
    }
    """
    When I request "/products" using HTTP POST
    Then the response code is 422
    And response item "[errors]" should exist
    And response item "[errors][0][title]" should contain "Bad data"
