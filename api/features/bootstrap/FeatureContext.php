<?php

use Symfony\Component\PropertyAccess\PropertyAccess;
use Assert\Assert;
use Behat\Gherkin\Node\TableNode;
use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Imbo\BehatApiExtension\Context\ApiContext;
use App\Service\Order\OrderFactory;
use Assert\AssertionFailedException;
use App\Service\ApiClient\CountryResolver;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends ApiContext
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var array
     */
    private $aliases = [];

    /**
     * @var TimeTravelMachine
     */
    private $timeTravelMachine;

    /**
     * FeatureContext constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct( EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
        $this->orderFactory = new OrderFactory();
    }

    /**
     * @Given /^response item "([^"]*)" should exist$/
     */
    public function responseItemShouldExist($propertyPathKey):void
    {
        $this->requireResponse();

        // Get the decoded response body and make sure it's decoded to an array
        $body = json_decode(json_encode($this->getResponseBody()), true);

        $propertyPath = PropertyAccess::createPropertyAccessor();
        $value = $propertyPath->getValue($body, $propertyPathKey);

        try {
            // Compare the arrays, on error this will throw an exception
            Assert::that($value)->notNull();
        } catch (AssertionFailedException $e) {
            throw new \RuntimeException(
                sprintf('Response item does not exist: %s', $propertyPath)
            );
        }
    }

    /**
     * @Given /^response item "([^"]*)" should contain "([^"]*)"$/
     */
    public function responseItemShouldContain($propertyPathKey, $expectedValue)
    {
        $this->requireResponse();

        // Get the decoded response body and make sure it's decoded to an array
        $body = json_decode(json_encode($this->getResponseBody()), true);

        $propertyPath = PropertyAccess::createPropertyAccessor();
        $value = $propertyPath->getValue($body, $propertyPathKey);
        $value = is_array($value) ? array_keys($value) : $value;
        $value = is_array($value) ? $value[0] : $value;
        Assert::that((string) $value)->contains($expectedValue);
    }

    /**
     * @Given /^I create products:$/
     */
    public function createProducts(TableNode $table)
    {
        foreach ($table->getHash() as $row) {
            $product = new Product();
            $product->setPrice($row['price']);
            $product->setProductType($row['product_type']);
            $product->setColor($row['color']);
            $product->setSize($row['size']);

            $this->entityManager->persist($product);
            $this->aliases[$row['id']] = $product;
        }
        $this->entityManager->flush();
    }

    /**
     * @Given /^I create order:$/
     */
    public function createOrder(TableNode $table)
    {
        $rows = $table->getHash();
        if (empty($rows)) {
            return;
        }

        $orderItems = [];
        foreach ($rows as $row) {
            $orderItem['quantity'] = $row['quantity'];
            $orderItem['product'] = $this->aliases[$row['product_id']];
            $orderItems[] = $orderItem;
            $createdAt = $row['created_at'] ?? 'now';
            $originCountry = $row['origin_country'] ?? CountryResolver::DEFAULT_COUNTRY_CODE;
        }

        $order = $this->orderFactory->create($orderItems);
        $order->setCreatedAt(new \DateTimeImmutable($createdAt));
        $order->setOriginCountry($originCountry);

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    /**
     * @Given /^the aliased request body is:$/
     */
    public function replaceAliasesAndSetRequestBody($string)
    {
        foreach (array_keys($this->aliases) as $aliasKey) {
            $string = str_replace($aliasKey, $this->aliases[$aliasKey], $string);
        }

        $this->setRequestBody($string);
    }

    /**
     * @BeforeScenario
     */
    public function cleanDatabaseAndClearAliases()
    {
        exec('php bin/console -e test doctrine:database:drop --force  \
            && php bin/console -e test doctrine:database:create --no-interaction \
            && php bin/console -e test doc:mig:mig --no-interaction --quiet');
        $this->aliases = [];
    }
}
