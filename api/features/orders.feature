Feature: Orders

  Scenario: Create draft order and calculate price
    Given I create products:
      | id             | price | product_type | color  | size |
      | <product_id_1> | 10.00 | mug          | green  | M    |
      | <product_id_2> | 15.50 | t-shirt      | black  | L    |
      | <product_id_3> | 5.25  | pin          | blue   | XS   |
    Given the aliased request body is:
    """
    [
        {
          "id": "<product_id_1>",
          "quantity" : "3"
        },
        {
          "id": "<product_id_2>",
          "quantity" : "2"
        },
        {
          "id": "<product_id_3>",
          "quantity" : "1"
        }
    ]
    """
    When I request "/orders" using HTTP POST
    Then the response code is 201
    And response item "[id]" should exist
    And response item "[status]" should contain "draft"
    And response item "[total_price]" should contain "66.25"

  Scenario: Restrict empty order creation
    Given the aliased request body is:
    """
    []
    """
    When I request "/orders" using HTTP POST
    Then the response code is 422
    And response item "[errors]" should exist
    And response item "[errors][0][title]" should contain "Empty order"

  Scenario: Restrict order creation having price less than 10
    Given I create products:
      | id             | price | product_type | color  | size |
      | <product_id_1> | 9.99  | pin          | blue   | XS   |
      | <product_id_4> | 1.25  | pin          | blue   | XXS  |

    Given the aliased request body is:
    """
    [
        {
          "id": "<product_id_1>",
          "quantity" : "1"
        }
    ]
    """
    When I request "/orders" using HTTP POST
    Then the response code is 422
    And response item "[errors]" should exist
    And response item "[errors][0][title]" should contain "Minimum total price is lower than 10.00"

    Given the aliased request body is:
    """
    [
        {
          "id": "<product_id_4>",
          "quantity" : "4"
        }
    ]
    """
    When I request "/orders" using HTTP POST
    Then the response code is 422
    And response item "[errors]" should exist
    And response item "[errors][0][title]" should contain "Minimum total price is lower than 10.00"

  Scenario: List all orders
    Given I create products:
      | id             | price | product_type | color  | size |
      | <product_id_1> | 10.00 | mug          | green  | M    |
      | <product_id_2> | 15.50 | t-shirt      | black  | L    |
      | <product_id_3> | 5.25  | pin          | blue   | XS   |
    And I create order:
      | product_id     | quantity |
      | <product_id_1> | 2        |
    And I create order:
      | product_id     | quantity |
      | <product_id_1> | 1        |
      | <product_id_2> | 1        |
    And I create order:
      | product_id     | quantity |
      | <product_id_1> | 1        |
      | <product_id_2> | 2        |
      | <product_id_3> | 4        |
    When I request "/orders"
    Then the response code is 200
    Then the response body is a JSON array of length 3

  Scenario: List all orders by product type
    Given I create products:
      | id             | price | product_type | color  | size |
      | <product_id_1> | 10.00 | mug          | green  | M    |
      | <product_id_2> | 15.50 | t-shirt      | black  | L    |
    And I create order:
      | product_id     | quantity |
      | <product_id_1> | 2        |
    And I create order:
      | product_id     | quantity |
      | <product_id_1> | 1        |
      | <product_id_2> | 1        |
    And I create order:
      | product_id     | quantity |
      | <product_id_2> | 2        |
    When I request "/orders?product_type=mug"
    Then the response code is 200
    Then the response body is a JSON array of length 2
