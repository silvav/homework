docker_up:
	@docker-compose up -d --remove-orphans

docker_down:
	@docker-compose down

composer_install:
	@docker-compose run php composer install

composer_install_prod:
	@docker-compose run php composer install --no-ansi --no-interaction --no-progress --no-scripts --optimize-autoloader

phpunit:
	@docker-compose exec php ./vendor/bin/simple-phpunit

behat:
	@docker-compose exec php ./vendor/bin/behat -c behat.yml.dist ./features/

db:
	@docker-compose exec php bash -c 'php bin/console -e test doctrine:database:drop --force; \
                                      php bin/console -e test doctrine:database:create --no-interaction; \
                                      php bin/console -e test doc:mig:mig --no-interaction --quiet'

install: docker_up composer_install db

install_prod: docker_up composer_install_prod db